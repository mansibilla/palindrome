package sheridan;

public class Palindrome {
	
	public static boolean isPalindrome(String input)
	{
		
		input = input.toLowerCase().replaceAll(" ", "");
		
		for (int i = 0, j = input.length() - 1; i < j; i++, j--) {
			if(input.charAt(i) != input.charAt(j))
				return false;
			
			
		}
		
		return true;
	}
	
	public static void main (String[] args) 
	{
		System.out.println(" Is Anna a palindrome? " + Palindrome.isPalindrome("anna")) ;
		System.out.println(" Is Race Car a palindrome? " + Palindrome.isPalindrome("race car")) ;
		System.out.println(" Is Mansi a palindrome? " + Palindrome.isPalindrome("mansi")) ;
		System.out.println(" Is Taco Cat a palindrome? " + Palindrome.isPalindrome("taco cat")) ;
	}
}
